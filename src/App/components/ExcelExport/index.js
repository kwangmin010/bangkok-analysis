import React, { Component } from "react";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const stock = (data) => (
        <ExcelSheet data={data} name="sheet1">
            <ExcelColumn label="일자" value="YYYYMMDDHHMM"/>
            <ExcelColumn label="매도" value="BUYPOINT_A"/>
            <ExcelColumn label="매수" value="SELLPOINT_A"/>
            <ExcelColumn label="매도" value="BUYPOINT_B"/>
            <ExcelColumn label="매수" value="SELLPOINT_B"/>
            <ExcelColumn label="차액_A" value="CHART_A"/>
            <ExcelColumn label="차액_B" value="CHART_B"/>
        </ExcelSheet>
);

const bitcoin = (data) => (
        <ExcelSheet data={data} name="sheet1">
            <ExcelColumn label="일자" value="date"/>
            <ExcelColumn label="코드" value="code"/>
            <ExcelColumn label="매도1호가" value={(row) => {
                return row.sell[0];
            }}/>
            <ExcelColumn label="매도2호가" value={(row) => {
                return row.sell[1];
            }}/>
            <ExcelColumn label="매도3호가" value={(row) => {
                return row.sell[2];
            }}/>
            <ExcelColumn label="매도4호가" value={(row) => {
                return row.sell[3];
            }}/>
            <ExcelColumn label="매도5호가" value={(row) => {
                return row.sell[4];
            }}/>
            <ExcelColumn label="매수1호가" value={(row) => {
                return row.buy[0];
            }}/>
            <ExcelColumn label="매수2호가"value={(row) => {
                return row.buy[1];
            }}/>
            <ExcelColumn label="매수3호가" value={(row) => {
                return row.buy[2];
            }}/>
            <ExcelColumn label="매수4호가" value={(row) => {
                return row.buy[3];
            }}/>
            <ExcelColumn label="매수5호가" value={(row) => {
                return row.buy[4];
            }}/>
        </ExcelSheet>
);

class ExcelExport extends Component {
    render() {
        return (
            <ExcelFile element={this.props.element}>
                {
                    this.props.divison == 'stock'? stock(this.props.data) : bitcoin(this.props.data)
                }
            </ExcelFile>
        )
    }
}

export default ExcelExport;