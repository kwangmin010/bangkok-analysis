export const drawerWidth = 240;

export const MENU_CONFIG = {
    A : {
        menu : {title : 'OKEX-선물', subTitleA : 'BTC 분기', subTitleB : 'BTC 차주', graph : true},
        param : {
            agentA : 'OKEX_CONTRACT', coinA : 'btc_usd', gubunA : 'quarter',
            agentB : 'OKEX_CONTRACT', coinB : 'btc_usd', gubunB : 'next_week'
        }
    },
    B : {
        menu : {title : 'OKEX-선물', subTitleA : 'BTC 분기', subTitleB : 'BTC 금주', graph : true},
        param : {
            agentA : 'OKEX_CONTRACT', coinA : 'btc_usd', gubunA : 'quarter',
            agentB : 'OKEX_CONTRACT', coinB : 'btc_usd', gubunB : 'this_week'
        }
    },
    C : {
        menu : {title : 'OKEX-선물', subTitleA : 'BTC 차주', subTitleB : 'BTC 금주', graph : true},
        param : {
            agentA : 'OKEX_CONTRACT', coinA : 'btc_usd', gubunA : 'next_week',
            agentB : 'OKEX_CONTRACT', coinB : 'btc_usd', gubunB : 'this_week'
        }
    },
    D : {
        menu : {title : '키움증권 자료 조회', subTitleA : null, subTitleB : null, graph : false},
        param : {
            agentA : null, coinA : null, gubunA : null,
            agentB : null, coinB : null, gubunB : null
        }
    }
}

