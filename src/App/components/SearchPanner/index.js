import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import ExcelExport from '../ExcelExport';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        backgroundColor : 'white',
        padding : 20,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 150,
    },
    button: {
        margin: theme.spacing.unit,
    },
    formControl: {
        minWidth: 200,
    },
});

class SearchPenal extends Component{
    state = {
        selectedCode : '',
        codes : [],
        fromDt : this.props.selectedMenu.fromDt,
        toDt : this.props.selectedMenu.toDt,
    };

    handleChange = event => {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        });
    };

    componentWillReceiveProps(nextProps) {
       if( this.props.selectedMenu.codes !== nextProps.selectedMenu.codes){
           this.setState({
               ...this.state,
               codes : [...nextProps.selectedMenu.codes]
           })
       }
    }

    render() {

        const { classes, selectedMenu, data, searchBtnClick } = this.props;

        return (
            <form className={classes.container} noValidate>
                <TextField
                    name="fromDt"
                    label="시작일"
                    type="date"
                    defaultValue={this.state.fromDt}
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={this.handleChange}
                />
                <TextField
                    name="toDt"
                    label="종료일"
                    type="date"
                    defaultValue={this.state.toDt}
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={this.handleChange}
                />

                {
                    !selectedMenu.graph ? (
                        <div>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="code">종목선택</InputLabel>
                                <Select
                                    value={this.state.selectedCode}
                                    onChange={this.handleChange}
                                    inputProps={{
                                        name: 'selectedCode',
                                    }}
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {
                                        this.state.codes.map((value, i) => <MenuItem key={i} value={value}>{value}</MenuItem>)
                                    }
                                </Select>
                            </FormControl>
                            <Button
                                variant="contained" color="secondary" className={classes.button}
                                onClick={() => searchBtnClick('getSymbol', this.state.fromDt, this.state.toDt)}
                            >종목 조회
                            </Button>
                        </div>
                    ) : null
                }
                <div>
                    <Button
                        variant="contained" color="primary" className={classes.button}
                        onClick={() => {
                            !selectedMenu.graph ?
                                searchBtnClick('getStockData', this.state.fromDt, this.state.toDt, this.state.selectedCode)
                                : searchBtnClick('getBitcoinData', this.state.fromDt, this.state.toDt)
                        }}
                    >자료 조회</Button>
                    <ExcelExport
                        division={selectedMenu.graph ? 'bitcoin' : 'stock'}
                        data={data}
                        element={<Button variant="contained" className={classes.button}>엑셀 다운</Button>}
                    />
                </div>
            </form>
        );
    }
}

SearchPenal.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchPenal);