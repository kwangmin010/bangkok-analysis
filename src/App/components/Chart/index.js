import React, {Component} from 'react'
import {Line} from 'react-chartjs-2';
import * as zoom from 'chartjs-plugin-zoom'

class Chart extends Component {
    getLableAndChartData = (data) => {

        let labels = [];
        let chartA = [];
        let chartB = [];

        data.map((value) => {
            if(value.CHART_A && value.CHART_B){
                labels.push(value.YYYYMMDDHHMM);
                chartA.push(value.CHART_A);
                chartB.push(value.CHART_B);
            }
        })

        return {
            labels: labels,
            datasets: [
                {
                    label: '차액_A',
                    backgroundColor: 'transparent',
                    borderColor: 'red',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: chartA
                },
                {
                    label: '차액_B',
                    backgroundColor: 'transparent',
                    borderColor: 'blue',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: chartB
                }
            ],
        }
    }

    render() {
        return (
            <Line
                data={this.getLableAndChartData(this.props.data)}
                options={{
                    maintainAspectRatio: true,
                    animation: {
                        duration: 0, // general animation time
                    },
                    hover: {
                        animationDuration: 0, // duration of animations when hovering an item
                    },
                    responsiveAnimationDuration: 0, // animation duration after a resize
                    pan: {
                        enabled: true,
                        mode: 'xy'
                    },
                    zoom: {
                        enabled: true,
                        mode: 'xy',
                        limits: {
                            max: 10,
                            min: 0.5
                        }
                    },
                    elements: {
                        point: {
                            radius: 0
                        }
                    }
                }}
            />
        )
    }
}

export default Chart;