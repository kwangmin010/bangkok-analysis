import React, {Component} from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css'

class DatSheet extends Component {

    render() {
        const { selectedMenu } = this.props;
        const stockColums = [
            {Header: "일자", accessor: "date"},
            {Header: "코드", accessor: "code"},
            {Header: "매도1호", accessor: "sell[0]", width:80},
            {Header: "매도2호", accessor: "sell[1]", width:80},
            {Header: "매도3호", accessor: "sell[2]", width:80},
            {Header: "매도4호", accessor: "sell[3]", width:80},
            {Header: "매도5호", accessor: "sell[4]", width:80},
            {Header: "매수1호", accessor: "buy[0]", width:80},
            {Header: "매수2호", accessor: "buy[1]", width:80},
            {Header: "매수3호", accessor: "buy[2]", width:80},
            {Header: "매수4호", accessor: "buy[3]", width:80},
            {Header: "매수5호", accessor: "buy[4]", width:80},
        ];

        const BitcoinColums = [
            {Header: "일자", accessor: "YYYYMMDDHHMM"},
            {
                Header: selectedMenu.subTitleA,
                columns: [
                    {Header: '매도', accessor: "SELLPOINT_A"},
                    {Header: "매수", accessor: "BUYPOINT_B"},
                ]
            },
            {
                Header: selectedMenu.subTitleB,
                columns: [
                    {Header: "매도", accessor: "SELLPOINT_B"},
                    {Header: "매수", accessor: "BUYPOINT_B"},
                ]
            },
            {Header: "차액_A", accessor: "CHART_A"},
            {Header: "차액_B", accessor: "CHART_B"},
        ]

        return (
            <ReactTable
                data={this.props.data}
                columns={selectedMenu.graph ? BitcoinColums : stockColums}
                defaultPageSize={10}
                className="-striped -highlight"
            />
        )
    }
}

export default DatSheet;