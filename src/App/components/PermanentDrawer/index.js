import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import { drawerWidth } from '../config';
import Header from '../Header';

class PermanentDrawer extends Component {
    state = {
        anchor: 'left',
    };

    render() {
        const { classes, menuClickHander } = this.props;
        const { anchor } = this.state;

        const drawer = (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor={anchor}
            >
                <div className={classes.toolbar} />
                <Divider />
                <List>
                    <ListItem button>
                        <ListItemIcon>
                            <ShowChartIcon />
                        </ListItemIcon>
                        <ListItemText primary="OKEX - 선물" />
                    </ListItem>
                    <ListItem button onClick={() => menuClickHander('A')}>
                        <ListItemText inset secondary="BTC 분기 - BTC 차주" />
                    </ListItem>
                    <ListItem button onClick={() => menuClickHander('B')}>
                        <ListItemText inset secondary="BTC 분기 - BTC 금주" />
                    </ListItem>
                    <ListItem button onClick={() => menuClickHander('C')}>
                        <ListItemText inset secondary="BTC 차주 - BTC 금주" />
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <ListItem button>
                        <ListItemIcon>
                            <ShowChartIcon />
                        </ListItemIcon>
                        <ListItemText primary="키움증권 자료 조회" onClick={() => menuClickHander('D')} />
                    </ListItem>
                </List>
            </Drawer>
        );

        let before = null;
        let after = null;

        if (anchor === 'left') {
            before = drawer;
        } else {
            after = drawer;
        }

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <Header title={'Bangkok Analysis'} anchor={anchor} />
                    {before}
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        {this.props.children}
                    </main>
                    {after}
                </div>
            </div>
        );
    }
}

PermanentDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        height:'100%'
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
});

export default withStyles(styles)(PermanentDrawer);