import axios from 'axios';

function makeFormData ( dataObj ) {
    let param = new URLSearchParams();
    for (let key in dataObj) {
        if(typeof(dataObj[key]) === 'string'){
            param.append(key, dataObj[key]);
        }else {
            param.append(key, JSON.stringify(dataObj[key]));
        }
    }
    return param;
}

const URL = '';

export const getStockSymbol = (params) => {
    return axios.post(URL+'/api/v1/stock/getSymbols', makeFormData(params))
};

export const getStockDataList = (params) => {
    return axios.post(URL+'/api/v1/stock/getData', makeFormData(params))
};

export const getBitcoinDataList = (params) => {
    return axios.post(URL+'/api/v1/bitcoin/getAnalysisData', makeFormData(params))
}
