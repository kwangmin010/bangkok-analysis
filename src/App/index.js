import React, {Component} from 'react';
import { PermanentDrawer, Chart, DatSheet, SearchPanner} from './components';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import { getStockSymbol, getStockDataList, getBitcoinDataList } from './service/api';
import { MENU_CONFIG } from './components/config';
import CircularProgress from '@material-ui/core/CircularProgress';

class App extends Component {

    state = {
        loading : false,
        menu : {
            title : null,
            subTitleA : null,
            subTitleB : null,
            fromDt : moment().subtract(10, 'days').format('YYYY-MM-DD'),
            toDt : moment().format('YYYY-MM-DD'),
            graph : true,
            codes : []
        },
        listDataStock : [],
        listDataBitcoin : [],
        param : {
            agentA : 'OKEX_CONTRACT', coinA : 'btc_usd', gubunA : 'quarter',
            agentB : 'OKEX_CONTRACT', coinB : 'btc_usd', gubunB : 'next_week'
        }
    };

    getBitcoinList = () => {
        this.setState({loading : true})
        getBitcoinDataList({
            fromDt : `${this.state.menu.fromDt.replace(/\-/g, '')}%`,
            toDt : `${this.state.menu.toDt.replace(/\-/g, '')}%`,
            ...this.state.param
        })
            .then(response => {
                let list = response.data.List;
                if(list){
                    this.setState({
                        listDataBitcoin : list,
                        loading : false
                    })
                }else{
                    alert(`자료가 검색되지 않았습니다`);
                    this.setState({loading : false})
                }
            }).catch(err => {
            alert(`API호출에 문제가 있습니다. ${err}`);
            this.setState({loading : false})
        });
    }

    componentDidMount() {
        this.menuClickHander('A');
        this.getBitcoinList();
    }

    searchBtnClick = (division, fromDt, toDt, code) => {
        let fDt = new Date(fromDt).getTime();
        let tDt = new Date(toDt).getTime();

        if(fDt < tDt) {
            this.setState({
                menu : {
                    ...this.state.menu,
                    fromDt : moment(fromDt).format('YYYY-MM-DD'),
                    toDt : moment(toDt).format('YYYY-MM-DD')
                }
            }, () => {
                if(division === 'getSymbol'){
                    getStockSymbol({
                        fromDt : `${fromDt.replace(/\-/g, '')}%`,
                        toDt : `${toDt.replace(/\-/g, '')}%`
                    })
                        .then(response => {
                            let codes = response.data.CODE;
                            if (codes) {
                                this.setState({
                                    menu : {
                                        ...this.state.menu,
                                        codes : codes
                                    }
                                }, () => {
                                    alert(`${codes.length}개의 코드가 검색 되었습니다`);
                                })
                            }else{
                                alert(`검색된 코드가 없습니다.`);
                            }
                        }).catch(err => {
                            alert(`API호출에 문제가 있습니다. ${err}`);
                        });
                }else if(division === 'getStockData'){
                    if(code){
                        getStockDataList({
                            fromDt : `${fromDt.replace(/\-/g, '')}%`,
                            toDt : `${toDt.replace(/\-/g, '')}%`,
                            code : code
                        })
                            .then(response => {
                                let list = response.data.list;
                                if(list){
                                    this.setState({listDataStock : list},
                                        () =>  alert(`${list.length}개의 레코드가 검색 되었습니다`))
                                }else{
                                    alert(`검색된 항목이 없습니다.`);
                                }

                            }).catch(err => {
                            alert(`API호출에 문제가 있습니다. ${err}`);
                        });
                    }else{
                        alert('종목코드가 선택되지 않았습니다');
                    }
                }else {
                    this.getBitcoinList();
                }
            });
        }else{
            alert("시작일이 종료일 보다 큽니다. 확인해 주세요!!");
        }
    };

    menuClickHander = (menuId) => {
        this.setState({
            ...this.state,
            menu : {...this.state.menu, ...MENU_CONFIG[menuId].menu},
            param : {...this.state.param, ...MENU_CONFIG[menuId].param}
        }, () => {
            if(menuId !== 'D'){
                this.getBitcoinList();
            }
        });

    };

    render() {
        return (
            <PermanentDrawer menuClickHander={this.menuClickHander}>
                <div>
                    <Typography variant="headline" gutterBottom>
                        <CircularProgress color="secondary" style={{'display' : this.state.loading ? '' : 'none'}}/>
                        {this.state.menu.title}
                    </Typography>
                    {
                        this.state.menu.graph ?
                            (<div>
                                <Typography variant="subheading" gutterBottom>
                                    {`${this.state.menu.subTitleA} - ${this.state.menu.subTitleB}  ( ${this.state.menu.fromDt} ~ ${this.state.menu.toDt} )`}
                                </Typography>
                                <Chart data={this.state.listDataBitcoin}/>
                            </div>): null
                    }
                    <p></p>
                    <SearchPanner
                        selectedMenu={this.state.menu}
                        data={this.state.menu.graph ? this.state.listDataBitcoin : this.state.listDataStock}
                        searchBtnClick={this.searchBtnClick}/>
                    <DatSheet
                        data={this.state.menu.graph ? this.state.listDataBitcoin : this.state.listDataStock}
                        selectedMenu={this.state.menu} />
                </div>
            </PermanentDrawer>
        )
    }
}

export default App;
